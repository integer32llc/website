---
layout: page
title: About
permalink: /about/
---

Integer 32 values high quality software and sees Rust as a great tool in any programmer's toolbox.
We want to use Rust to make software faster and crash less often.
Software reliability and maintainability are important to us and baked into every project we do.

We believe a thriving community is important for successful technology projects.
We make time to contribute back to the community through code, learning resources, and events.
By choosing to work with us, you are subsidizing our work with the community and ensuring a healthy ecosystem for years to come.

Proudly Rust Belt built, we incorporated in Pittsburgh, PA in 2016.
We operate as a collective, and intend to continue to do so as we grow.
We believe it's important for people working together to be equally invested in the business and outcomes.

## The Team

<div class="team-member">
    <h3>Carol Nichols</h3>

    <img src="/images/carol-nichols-headshot-badge.png" class="headshot" alt="Carol Nichols" />

    <ul>
        <li><a href="https://www.rust-lang.org/team.html">Member of the Rust Core Team, Tools team, and Community Team</a></li>
        <li>Co-author of the upcoming revision of <a href="https://www.nostarch.com/rust">The Rust Programming Language</a> book</li>
        <li>Maintainer of <a href="https://github.com/carols10cents/rustlings">Rustlings</a></li>
        <li><a href="http://www.rust-belt-rust.com/">Rust Belt Rust Conference</a> Organizer</li>
        <li><a href="http://confreaks.tv/videos/rustcamp2015-navigating-the-open-seas">Spoke at RustCamp 2015</a></li>
    </ul>
</div>

<div class="team-member">

    <h3>Jake Goulding</h3>

    <img src="/images/jake-goulding-headshot-badge.png" class="headshot" alt="Jake Goulding" />

    <ul>
        <li><a href="http://stackoverflow.com/tags/rust/topusers">#1 in the Rust tag on Stack Overflow</a></li>
        <li>Creator of the <a href="http://jakegoulding.com/rust-ffi-omnibus/">Rust FFI Omnibus</a></li>
        <li>Maintainer of the <a href="https://crates.io/crates/jetscii">jetscii</a>, <a href="https://crates.io/crates/twox-hash">twox-hash</a>, <a href="https://crates.io/crates/sxd-document">sxd-document</a>, <a href="https://crates.io/crates/sxd-xpath">sxd-xpath</a>, <a href="https://crates.io/crates/cupid">cupid</a>, and <a href="https://crates.io/crates/peresil">peresil</a> crates</li>
        <li><a href="http://www.rust-belt-rust.com/">Rust Belt Rust Conference</a> Organizer</li>
    </ul>
</div>

## Company purpose

Integer 32’s purpose is to cultivate technological advancement, and in doing so establish technologists as the collective owners of the means of production.
The Company will operate with the goal of being ethically responsible, producing high quality technical solutions intended to inspire others to work towards a similar purpose.
The Company is committed to the enhancement of open source technology, community enrichment and training, and will offer opportunity to marginalized members of the communities in which we operate.
Everything the Company and its contributors produce should be designed with the goal of having a lasting, meaningful effect on the users of its technology and all those involved in its production.
