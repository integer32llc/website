---
layout: post
title:  Crates.io for the Enterprise
date:   2017-03-21
author: Carol
---

Here at Integer 32, we've noticed some people working at larger software
companies mention that Rust is a tough sell. Much of the Rust ecosystem assumes
that you can install crates directly from crates.io, and that all crates are
open source. Company policies are sometimes in conflict with these assumptions.
**We want to provide a solution to address these issues and are looking for a
sponsor. Is that your company?**

## Current solutions

[cargo-vendor][] and [cargo-local-registry][] help when you are restricted from
installing crates over the internet, such as from build servers. They're
optimized for caching your dependencies locally in a filesystem, but don't
offer a web frontend or any authentication.

[cargo-vendor]: https://github.com/alexcrichton/cargo-vendor
[cargo-local-registry]: https://github.com/alexcrichton/cargo-local-registry

It is possible to install crates.io's codebase elsewhere, but being able to
swap out integrated components like GitHub and S3 isn't possible right now
without editing the code.

The public instance of crates.io doesn't have a way to host private crates or
manage who has permission to download them.

## Seeking sponsors

There are many different directions we could go with this product and/or
service. Most of all, we want to know that what we build will be useful to
someone who wants to adopt Rust but can't currently.

To that end, we're currently looking to get in touch with companies interested
in sponsoring this work. A sponsor would determine our priorities: we'd build
exactly what your company needs first, and work with you to ensure it meets
your requirements. This sponsorship would include either a year of free usage
(if we go with the SaaS version) or a year of support and updates (if we go
with a product installed within your firewall).

If this partnership possibility piques your interest, [let's chat][email] about
your needs and discuss sponsorship costs.

<a class="button" href="mailto:info@integer32.com">Email us about sponsoring</a>

[email]: mailto:info@integer32.com

## Seeking input

If using Rust in your enterprise is only a far-off possibility, we can
still use your help! If lack of enterprise-friendly infrastructure is *one* of
the factors preventing Rust adoption, please [fill out this survey][survey] to
help us assess where our efforts would be best spent! Survey responses will
only be used internally to inform Integer 32's direction, and you'll only need
to provide contact information if you'd like to stay in touch with us about our
progress.

<a class="button" href="https://www.surveymonkey.com/r/JK3BDTP">Take the survey</a>

[survey]: https://www.surveymonkey.com/r/JK3BDTP

## Subscribe to our newsletter

If you would like to subscribe to our Enterprise Rust newsletter without
completing the survey, or without associating your survey responses with your
contact information, use the form below. The newsletter will be sent out no
more than monthly, and will contain information on our product offerings, Rust
in the Enterprise tips, and more.

<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
    #mc_embed_signup form { padding: 0; }
    #mc_embed_signup .button { background-color: #354C9F; }
</style>
<div id="mc_embed_signup">
<form action="//rust-belt-rust.us4.list-manage.com/subscribe/post?u=b0dd9ed5f4fad13c850ed6dd3&amp;id=aada5e776c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">Subscribe to our mailing list</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_b0dd9ed5f4fad13c850ed6dd3_aada5e776c" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->
