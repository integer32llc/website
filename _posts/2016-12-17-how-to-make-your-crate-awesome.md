---
layout: post
title:  Six Easy Ways to Make Your Crate Awesome
date:   2016-12-27
author: Carol
---

Recently, we ran a survey to find out how Rustaceans evaluate crates on
crates.io in order to inform [an RFC on the default order of crates][rfc]
within a category or keyword. Regardless of what happens with that RFC, the
survey responses contained a lot of information that crate authors can use
to help people who are evaluating crates. The following are some low-effort
actions crate authors can take to make it more likely that users will consider
their crate. Number 4 will shock you!!!!!!!

This list assumes that you *want* other people to use your crate. If that's
why you wrote and released it, keep reading! If you wrote a crate as an
experiment, or if you've chosen to no longer maintain a crate you've published,
that's totally fine too! You might actually want to do the *opposite* of
these recommendations if you want to discourage people from using your crate
in most circumstances.

Part of what's exciting to me is the amount of opportunity available in the
Rust ecosystem to help each other by writing and releasing libraries. Even
small crates can provide really useful functionality for everyone to use,
provided that potential users are able to find it, figure out how to use it,
and determine if it's appropriate for their use cases.

## 1. Documentation, documentation, documentation

By far, the most mentioned attribute of a crate that crate users were looking
for was good documentation. This doesn't necessarily mean that every public
item has docs, but the top-level items in your public API should have a
description of what you would use them for, and ideally also have an example.
Other kinds of documentation, such as tutorials, were also highly valued and
made it more likely that a crate user would try out your crate. Make sure links
to those are easily findable, such as near the top of your README!

So add API docs to your crate, with examples where appropriate. Publish a
version on crates.io and [Docs.rs][] will host the rendered docs for you! Until
[crates.io links to Docs.rs automatically][459], specify your crate's Docs.rs
URL as the documentation link in `Cargo.toml` to make it easy for potential
users to find your crate.

## 2. Add an example to top-level module docs and the README

Along with detailed API documentation, people evaluating crates were looking for
something that would give them a sense of what it would be like to use this
crate. They mentioned looking for an end-to-end example with prose or
comments explaining what the code was doing.

Currently, there's not a consensus on where the majority of the introductory
documentation should go: in the README that's rendered on the repository, or
in the crate's top-level module docs so that it shows up on the front page of
the documentation? Given that crates.io doesn't render the README [yet][], and
that not all crates have documentation, survey respondents mentioned checking
in both places. For that reason, until there's a better solution, I'd recommend
duplicating the example and explanation in both these locations.

## 3. Set all applicable fields in Cargo.toml

You're required to put some content in the `description`, `license`, and
`authors` fields before publishing on crates.io, but it's worth thinking about
the content of those fields and whether adding some optional metadata would
help people find and evaluate your crate.

Without looking, do you know where your crate's `description` is shown on
crates.io? It's on your crate's page, and it's also used in lists of crates
such as search results or within a keyword. That makes the `description` field
a great place to pitch why your crate is useful and what distinguishes it from
other crates.

Definitely link to your documentation in the `documentation` link field, as
discussed above. Consider *not* adding a `homepage` link unless you have a
homepage that's different than your repository and documentation links-- a few
survey respondents expressed frustration at looking through all the links and
hoping for an informative homepage but finding out it's the same as another
page.

If you have a README file, specify the filename in the `readme` field. If you
do, crates.io will index the text in the README file so that searches on
crates.io can use that info, which will make your crate easier to find if
people search for it using words that only appear in your README. Additionally,
Docs.rs uses the readme field to render the README file specified on its pages,
and crates.io might do the same in the future.

Also consider adding up to 5 [keywords][], which are indexed for searching as
well. Keywords will likely be made [more visible for browsing][browse] someday
too! Check out [categories][] when those are released; the list of categories
will be managed via pull request to crates.io, and the categories won't be
used for search. They're designed solely to organize crates by their purpose,
whereas keywords can be used to indicate any attribute of a crate, and crate
authors can specify any keyword they want.

## 4. Release a 1.0 version

Many crate users are looking for a signal of production readiness and stability
that a version >= 1.0 indicates. If you're using your own crate in production,
and if you don't have any planned rearranging of your public API, why not
release a 1.0 version? Besides, even if you do decide the public API should
be changed, you can release a new major version. That's what semver is for!

On the other hand, if you *don't* want people to use your crate in production
for whatever reason, then leaving the version number as something less than 1.0
is a great way to signal that.

## 5. Write a blog post about it

People might not need your crate right now, but they might in the future. If
they've heard of your crate before when they do find themselves in a situation
your crate is useful, they'll either go straight to your crate or recognize it
in a list of crates and be more likely to take another look.

Link to your post in [the users forum][urlo], [the Rust subreddit][r/rust],
and on twitter with the #rustlang hashtag to tell everyone about it.
Self promotion isn't a bad thing!

As a bonus, you can link to the blog post about your crate from your README
as extra documentation!

## 6. Set up TravisCI and Appveyor

[TravisCI] and [Appveyor] are great ways to automatically run your tests every
time you push new code. It's important to set up both: TravisCI can run your
tests on Linux and macOS, while Appveyor can run them on Windows. If your crate
works on all platforms, show that off! Add the badges provided by these
services to your README to make it easy for potential users to know that you
have tests, that they're passing, and on what platforms the tests are passing.

[rfc]: https://github.com/rust-lang/rfcs/pull/1824
[Docs.rs]: https://docs.rs/
[459]: https://github.com/rust-lang/crates.io/pull/459
[yet]: https://github.com/rust-lang/crates.io/issues/81
[urlo]: https://users.rust-lang.org/
[r/rust]: http://www.reddit.com/r/rust
[TravisCI]: http://travis-ci.org/
[Appveyor]: https://www.appveyor.com/
[keywords]: https://crates.io/keywords
[browse]: https://github.com/rust-lang/crates.io/issues/490
[categories]: https://github.com/rust-lang/crates.io/pull/473
