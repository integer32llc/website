---
layout: post
title:  More Rust in 2018
date:   2018-01-05
author: Carol
---

Recently, the Rust teams [put out a call for community thoughts on what Rust’s 2018 goals should
be](https://blog.rust-lang.org/2018/01/03/new-years-rust-a-call-for-community-blogposts.html), and
we’d like to weigh in from our perspective as a Rust-focused consultancy.

What we’d love to see from the teams is more of a focus on **driving Rust adoption in production**.
The focus on productivity in 2017 was much-needed groundwork, and we think it’s time to trumpet
from the mountaintops what the Rust community has known for a while: **Rust is production ready**.

## Why drive Rust adoption?

One reason we started a Rust consultancy is because we believe using Rust can help make better
quality software. That improved quality only comes if we convince more people to use Rust! In
addition to increasing business for consultancies (our obvious angle here), more companies using
Rust means more full-time Rust positions, more feedback for the community informed by real-world
use cases, and more contributions back to the ecosystem from companies, whether that be open source
libraries or sponsorship.

## How do we drive Rust adoption?

The core team has had calls with some [current production
users](https://www.rust-lang.org/en-US/friends.html), and every time we’ve learned a great deal
about how Rust is actually working. These calls have covered rough edges, missing parts of the
ecosystem, high-impact bugs, and also where Rust really shines. In 2018, the core team (and other
teams as appropriate) should do more of those calls. Additionally, more venues should be created to
**work with production users to gather regular feedback** in a convenient, scalable way. The Rust
teams should market these channels so that new and potential production users can find the support
they need!

One of the 2017 roadmap goals was to lower the initial learning curve. Through efforts such as
completing [The Rust Programming Language](https://doc.rust-lang.org/book/second-edition/) book,
improving documentation, and creating resources like [The Rust
Cookbook](https://rust-lang-nursery.github.io/rust-cookbook/), the initial onboarding experience is
getting smoother! What comes next? Once you have a project in Rust, what are good options for
deployment? Logging? Debugging? Managing dependencies? We, as a community, should work on creating
**the next level of learning resources to help folks deploy Rust to production with confidence.**

Here’s hoping for more production Rust usage in 2018!
