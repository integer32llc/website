---
layout: post
title: "Tech Conference Budget Case Study: Rust Belt Rust 2018"
date:   2018-11-29
author: Carol
---

The following is a financial report for Rust Belt Rust 2018 for anyone interested in what it takes
to run a similar conference. There are many choices conference organizers have that impact
budget, and we wanted to show what our choices cost as a data point for anyone considering running
their own tech conference!

## RBR 2018 Facts & Figures

A bit of background on the conference:

- [Rust Belt Rust](https://www.rust-belt-rust.com/) is in its third year - it has been held in Pittsburgh, Columbus, and Ann Arbor.
- Integer 32 runs the conference, but it is supported by sponsorships from other companies and individuals.
- It is a two-day conference, with workshops on day 1 and a single track of half-hour talks on day 2.
- Ticket cost this year: $200 for full-price tickets. There were early bird and student discounts.
- We had about 130 people in attendance, which includes ticketed attendees, speakers, diversity scholarship recipients, and organizers.

## TL;DR: Totals

We aim for the conference to be cost neutral. So how did we do?

- Total income: $24,880.00
- Total expenses: -$22,985.80
- Net: +$1,894.20

Pretty much spot on!

## Income

The majority of our income came from ticket sales:

<img src="/images/posts/2018-rust-belt-rust-finance-report/income.png" alt="Pie chart showing breakdown of income sources" style="width: 600px" />

| Source | Amount |
|--------|--------|
| Ticket Sales | $15,730.00 |
| Corporate Sponsorship | $8,200.00 |
| Individual Sponsorship | $950.00 |
| Total | $24,880.00 |

Individual sponsorships are people who opted to pay more than the listed ticket price to support
the conference.

In addition to these items that have price tags attached to them, we also received many valuable
in-kind donations:

- HealPay - hosted a party
- Organizers - volunteering time
- Employers of speakers - travel sponsorships

As Rust corporate usage grows, I'd love to have more support from companies to be able to lower
ticket prices and make the conference more accessible to more folks.

## Expenses

Explanation of the categories below:

<img src="/images/posts/2018-rust-belt-rust-finance-report/expenses.png" alt="Pie chart showing breakdown of expense categories" style="width: 600px" />

| Category | Amount |
|----------|--------|
| Speaker travel | $9,793.32 |
| Scholarship travel | $3,119.93 |
| Food | $2,326.62 |
| Attendee Stuff | $1,917.22 |
| Video recording | $1,643.54	|
| Space | $1,500.00 |
| Childcare | $480.00 |
| Insurance | $309.00 |
| Fees | $261.51 |
| Supplies | $194.47 |
| Software | $38.30 |
| Total | $22,985.80 |

RBR pays for speakers' transportation and hotel costs. This was, by far, our largest expense, but
an important one! We're honored to have folks from all over the world (as well as from the Rust
Belt) willing to speak. A few speakers had employers willing to cover their travel cost, which is
very helpful! I hope that increases as companies hire Rust developers and want to recruit more
developers by having their employees speak at conferences. We don't want to limit our speakers only
to people who have those resources or personal resources for travel, so this will continue to be a
large expense for us going forward.

We also awarded four diversity scholarships that cover transportation and hotel costs as well as a
free ticket. Helping people who are underrepresented in tech become a part of the Rust community is
important to us.

Another category to call out is food, which consisted of snacks, soda, tea, and a LOT of coffee,
which weighed in at 10% of our budget. We don't include meals and instead send everyone out for
lunch on their own, which saves us what would be a huge expense for food that might not be the
quality or type that all attendees enjoy-- this way, everyone gets to make their own decision on
what they eat. If you're considering running a conference and decide to provide meals, be prepared
to spend a LOT more on food than we did!

The space rental for our venue was also quite affordable, and the space we had available worked out
really well for the workshops, single track, and unconference. However, the hotel's location was a
few miles outside of downtown Ann Arbor, which made going out to lunch and out in the evenings a
bit more logistically tricky. I'll probably be spending more for a more urban location next year.

Attendee stuff includes bags, nametags, and swag. Fees includes Eventbrite ticket processing fees,
Eventbrite credit card processing fees, and wire transfer fees from reimbursing international
speakers.

A large expense that we didn't have for RBR that I have had in the past for another conference I
helped run is a party in the evening. While I enjoyed having one space where most attendees
socialized between the two days of the conference, I think not having an official party is the
right decision for Rust Belt Rust. We've had sponsors decide they wanted to hold an event; this
year HealPay hosted a gathering, which was awesome!

## Conclusion

I make particular budgetary choices when running a conference, which gives the event its culture.
Other events make different choices and have different feelings. I'd love to see more regional Rust
conferences with their own sets of choices. I know there's a lot of unknowns when running a
conference for the first time, so I hope this post helps to demystify the monetary part of a
conference and helps folks be confident in jumping into the conference organizing world.

If you're considering running a Rust conference, or any tech conference, and have additional
questions, [please reach out!](carol.nichols@integer32.com)

*Many thanks to Jean Lange for her help writing this up!*
