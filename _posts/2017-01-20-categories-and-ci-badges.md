---
layout: post
title:  Categories and CI badges are now available on crates.io!
date:   2017-01-20
author: Carol
---

We're pleased to announce that [support for categories][categories-pr] and
[support for Travis CI and Appveyor badges][badges-pr] has been deployed to
crates.io! In the near future, we're going to be sending pull requests to some
crates in order to add categories and CI badges, and then linking to the list
of categories from the homepage to make categories more visible.

## Purpose of categories

Categories are meant to help the crate user who is coming to crates.io with the
goal "I want a crate that does ______". This person should be able to go to
[crates.io/categories][categories], choose the category that corresponds to the
thing they want a crate to do, and find a list of crates for consideration
whose primary purpose is exactly that. Categories are one step in [the 2017
roadmap][roadmap] goal of providing easy access to high quality crates.

## How Categories are Different from Keywords and Searching

Categories are meant to augment [keywords][] rather than replace them.
Categories facilitate *browsing*, while keywords assist more
in *searching*. Keywords are indexed as part of the text that informs searches,
while categories are not. The categories available on crates.io are a curated
list maintained in the source of crates.io (and can be modified with a PR, see
"How do I suggest more categories?" below). This is a slightly higher barrier
than adding a new keyword: a crate author can start using a keyword at any
time with any meaning; categories are solely to enable browsing by
crate purpose. For instance, many crates that offer a command line
interface have the keyword "cli", but crates in the command-line
interface category should only be those that help a crate user build
their own CLI.

If you're a crate user who's looking for something very specific that there's
likely to only be a few crates for, searching will still be more useful than
categories. For example, you may be looking for a crate to interface with
[Telegram's](https://telegram.org/) API, so you'd search for
[telegram](https://crates.io/search?q=telegram) and find 7 results. Interfacing
with Telegram is a very focused goal, though, and is never likely to encompass
as many crates as, say, all the crates available to process text.

Which brings us to another difference: when you do a search on crates.io,
results are returned in relevancy order based on how often your search term
appears in crates' names, keywords, descriptions, and README. When you instead
browse to the list of crates that are in a category (or that use a keyword), we
have an opportunity to order crates by some metric other than search relevancy.
[We're still working with the community on what that ordering should be
exactly][rfc1824], but we have an opportunity to make recommendations to new
Rustaceans about the most awesome crates for a particular purpose.

## How do I categorize my crate and display its CI badges?

Keep in mind that you don't have to add *any* category to your crate!
Categories are for discovery of crates for any user who is browsing with a
particular purpose in mind. If your crate's purpose is, say, an implementation
detail of another crate like
[serde_codegen_internals](https://crates.io/crates/serde_codegen_internals), it
probably *shouldn't* have a category!

CI badges are also not required, but are a great way to show off that you have
tests and that they're passing. Appveyor, specifically, is an important
indication of Windows support!

You can add categories and CI badges to your crate today in 4 easy steps.
Here's how:

1. You'll need a nightly cargo newer than 2017-01-18, so that you have the PRs
supporting uploading of [category][cargo-cat-pr] and [badge][cargo-badges-pr]
metadata that appears in your Cargo.toml.

2. Choose the categories you'd like your crate to be in (each crate can have up
to 5 categories), and specify the slugs corresponding to those categories in
your Cargo.toml. See [the list of all valid slugs][slugs]; we chose this
implementation so that we can more easily change the display text of a
category, including potentially translating it. Here's an example:

       categories = ["multimedia::audio", "compression"]

    If you specify a slug that isn't in the list, cargo will print a warning
    when you publish but the publish will still go through with any valid
    categories.

3. Specify your Travis CI and Appveyor badges. Travis CI requires specifying a
repository, and you can optionally specify a branch; the default is `master`.
Appveyor requires specifying a repository as well, and similarly defaults to
the `master` branch, plus you can also optionally specify the service where
your repository is hosted: valid values are `github` and `bitbucket`, with the
default being `github`. Here's an example:

       [badges]
       travis-ci = { repository = "carols10cents/sassers" }
       appveyor = { repository = "carols10cents/sassers" }

4. You'll need to publish a new version. [We'd like to have a cargo command to
update crates.io metadata without publishing a new version][no-publish], but
that hasn't been implemented yet.

## How do I suggest more categories?

The list of categories, their slugs, and their descriptions lives in [a
categories.toml file on crates.io][categories.toml]. There are instructions at
the top of the file for sending in a pull request to suggest changes. You may
be interested in [the community process that decided on the initial
set][category-pr]-- we went through quite a few iterations!

## Future work

We don't consider the goal of providing easy access to high quality crates to
be met solely by adding categories, far from it! There's lots more work to do
to make crates.io more useful so that people can find the excellent crates in
Rust's ecosystem and be more productive. This work includes, but is not limited
to:

* [Making categories and keywords more
  visible](https://github.com/rust-lang/crates.io/issues/490)
* Badges for other axes of classifying crates. Ideas: whether crates work on
  nightly and/or stable, whether the crate is a library, binary, or both, level
  of stability/maintenance
* [Default ordering within a category (or keyword) other than
  downloads][rfc1824]
* More ways to navigate/refine/filter like [getting the intersection of
  keywords/categories](https://github.com/rust-lang/crates.io/issues/489) or
  [searching within a
  keyword/category](https://github.com/rust-lang/crates.io/issues/491)
* [Being able to update categories and other metadata for a crate without
  publishing a new version][no-publish]

We'll likely be working on some of these soon. If you'd like to help and if you
have any questions, please let us know!

[categories-pr]: https://github.com/rust-lang/crates.io/pull/473
[badges-pr]: https://github.com/rust-lang/crates.io/pull/504
[categories]: https://crates.io/categories
[cargo-cat-pr]: https://github.com/rust-lang/cargo/pull/3301
[cargo-badges-pr]: https://github.com/rust-lang/cargo/pull/3546
[slugs]: https://crates.io/category_slugs
[categories.toml]: https://github.com/rust-lang/crates.io/blob/master/src/categories.toml
[category-pr]: https://github.com/rust-lang/crates.io/pull/488
[keywords]: https://crates.io/keywords
[roadmap]: https://github.com/rust-lang/rfcs/blob/master/text/1774-roadmap-2017.md
[rfc1824]: https://github.com/rust-lang/rfcs/pull/1824
[no-publish]: https://github.com/rust-lang/cargo/issues/971
