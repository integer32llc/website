---
layout: default
permalink: /blog/
---

<div class="home">
  <p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a></p>

  <h1 class="page-heading">Blog</h1>

  <div class="posts">
    {% for post in site.posts %}
        <div class="post">
            <h2 class="post-title">
              <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
            </h2>
            <p class="post-meta">
                <span class="post-author">
                    by {{ post.author }},
                </span>

                <span class="post-date">
                    posted on <time datetime="{{ post.date | date_to_xmlschema }}" itemprop="datePublished">{{ post.date | date: "%b %-d, %Y" }}</time>
                </span>
            </p>

            <div class="post-excerpt">
                {{ post.excerpt }}
            </div>

            <span class="read-more">
                <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">Read more...</a>
            </span>
        </div>
    {% endfor %}
  </div>

  <p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a></p>

</div>
