---
layout: page
title: Contact
permalink: /contact/
---

<form class="contact" action="https://formspree.io/info@integer32.com" method="POST">
  <input type="hidden" name="_next" value="//integer32.com/thanks" />
  <input type="hidden" name="_subject" value="Integer 32 Inquiry" />
  <input type="text" name="_gotcha" style="display:none" />
  <label>
    <span class="required">Name:</span>
    <input type="text" name="name" required />
  </label>
  <label>
    <span class="required">Email:</span>
    <input type="email" name="_replyto" required />
  </label>
  <label>
    <span class="required">What can Integer 32 do for you?</span>
    <textarea name="description" required ></textarea>
  </label>
  <label>
    <span>Why are you interested in Rust?</span>
    <textarea name="whyrust"></textarea>
  </label>
  <label>
    <span>What is your budget?</span>
    <input type="text" name="budget" />
  </label>
  <label>
    <span>How did you hear about Integer 32?</span>
    <input type="text" name="reference" />
  </label>
  <input type="submit" value="Send" />
</form>
